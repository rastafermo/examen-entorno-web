class custom {
  file { '/var/www/prod/index.php':
    ensure => file,
    replace => true,
    content => "Hello world. Sistema operativo ${operatingsystem} ${operatingsystemrelease}",
    require => File ["/var/www/prod"]
  }
  file { "/var/www/prod":
    ensure => "directory",
  }
  file { '/var/www/dev/info.php':
    ensure => file,
    replace => true,
    source  => "puppet:///modules/custom/info.php",
    require => File ["/var/www/dev"]
  }
  file { "/var/www/dev":
    ensure => "directory",
  }
}

